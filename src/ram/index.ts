import { Byte, Word } from '../helper'

export default class Ram {
  ram: Uint8Array

  constructor (size: number) {
    this.ram = new Uint8Array(size)
    this.reset()
  }

  reset () {
    this.ram.fill(0)
  }

  read (addr: Word): Byte {
    return this.ram[addr]
  }

  write (addr: Word, data: number) {
    this.ram[addr] = data
  }
}
