import { dict } from '../cpu/opcode'
import log from '../helper'

export default class Debugger {
  debugInfo?: Array<Array<string | number>>

  constructor () {
    (window as any).__disassembled = () => this.displayDisassembled()
  }

  setup (rom: Uint8Array) {
    const debugInfo = []
    let pc = 0
    let opcodeIndex = 0
    while (rom[pc]) {
      const op = []
      const opcodeAddr = (0x8000 + pc).toString(16)
      const opcode = dict[rom[pc].toString(16).toUpperCase()]
      if (!opcode) {
        debugInfo[opcodeIndex] = [opcodeAddr]
        pc++
        opcodeIndex++
        continue
      }
      op.push(opcode.fullName)
      pc++
      switch (opcode.mode) {
        case 'accumulator':
        case 'implied':
          debugInfo[opcodeIndex] = [opcodeAddr, ...op]
          opcodeIndex++
          continue
      }
      op.push(rom[pc])
      pc++
      switch (opcode.mode) {
        case 'immediate':
        case 'relative':
        case 'zeroPage':
        case 'zeroPageX':
        case 'zeroPageY':
        case 'preIndexedIndirect':
        case 'postIndexedIndirect':
          debugInfo[opcodeIndex] = [opcodeAddr, ...op]
          opcodeIndex++
          continue
      }
      op.push(rom[pc])
      debugInfo[opcodeIndex] = [opcodeAddr, ...op]
      opcodeIndex++
      pc++
    }
    this.debugInfo = debugInfo
  }

  displayDisassembled () {
    if (this.debugInfo) this.debugInfo.forEach(log.debug)
  }
}
