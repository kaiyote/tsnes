import Apu from '../apu'
import CpuBus from '../bus/cpu-bus'
import PpuBus from '../bus/ppu-bus'
import Cpu from '../cpu'
import Debugger from '../debugger'
import Dma from '../dma'
import Interrupts from '../interrupts'
import Keypad from '../keypad'
import { parse } from '../parser'
import Ppu from '../ppu'
import Ram from '../ram'
import CanvasRenderer from '../renderer'
import Rom from '../rom'

export default class NES {
  cpu?: Cpu
  ppu?: Ppu
  apu?: Apu
  cpuBus?: CpuBus
  characterMem?: Ram
  programROM?: Rom
  ram?: Ram
  ppuBus?: PpuBus
  canvasRenderer: CanvasRenderer
  keypad?: Keypad
  dma?: Dma
  interrupts?: Interrupts
  debugger?: Debugger

  constructor () {
    this.frame = this.frame.bind(this)
    this.canvasRenderer = new CanvasRenderer('nes')
  }

  //
  // Memory map
  /*
  | addr           |  description               |   mirror       |
  +----------------+----------------------------+----------------+
  | 0x0000-0x07FF  |  RAM                       |                |
  | 0x0800-0x1FFF  |  reserve                   | 0x0000-0x07FF  |
  | 0x2000-0x2007  |  I/O(PPU)                  |                |
  | 0x2008-0x3FFF  |  reserve                   | 0x2000-0x2007  |
  | 0x4000-0x401F  |  I/O(APU, etc)             |                |
  | 0x4020-0x5FFF  |  ex RAM                    |                |
  | 0x6000-0x7FFF  |  battery backup RAM        |                |
  | 0x8000-0xBFFF  |  program ROM LOW           |                |
  | 0xC000-0xFFFF  |  program ROM HIGH          |                |
  */

  load (nes: ArrayBuffer) {
    const { characterROM, programROM, isHorizontalMirror } = parse(nes)
    if (process.env.NODE_ENV !== 'production') {
      const nesDebugger = new Debugger()
      nesDebugger.setup(programROM)
    }
    const ppuConfig = { isHorizontalMirror }
    this.keypad = new Keypad()
    this.ram = new Ram(2048)
    this.characterMem = new Ram(0x4000)
    // copy characterROM into internal RAM
    for (let i = 0; i < characterROM.length; i++) this.characterMem.write(i, characterROM[i])
    this.programROM = new Rom(programROM)
    this.ppuBus = new PpuBus(this.characterMem)
    this.interrupts = new Interrupts()
    this.apu = new Apu(this.interrupts)
    this.ppu = new Ppu(this.ppuBus, this.interrupts, ppuConfig)
    this.dma = new Dma(this.ram, this.ppu)
    this.cpuBus = new CpuBus(this.ram, this.programROM, this.ppu, this.keypad, this.dma, this.apu)
    this.cpu = new Cpu(this.cpuBus, this.interrupts)
    this.cpu.reset()
  }

  frame () {
    if (!this.dma || !this.cpu || !this.ppu || !this.apu) throw new Error('You need to run load before you start')
    while (true) {
      let cycle = 0
      if (this.dma.isDmaProcessing) {
        this.dma.runDma()
        cycle = 514
      }
      cycle += this.cpu.run()
      const renderingData = this.ppu.run(cycle * 3)
      this.apu.run(cycle)
      if (renderingData) {
        this.canvasRenderer.render(renderingData)
        break
      }
    }

    requestAnimationFrame(this.frame)
  }

  start () {
    requestAnimationFrame(this.frame)
  }

  close () {
    if (this.apu) this.apu.close()
  }
}
