import { Byte, Word } from '../helper'
import Ram from '../ram'

export default class PpuBus {
  characterRam: Ram

  constructor (characterRam: Ram) {
    this.characterRam = characterRam
  }

  readByPpu (addr: Word): Byte {
    return this.characterRam.read(addr)
  }

  writeByPpu (addr: Word, data: Byte) {
    this.characterRam.write(addr, data)
  }
}
