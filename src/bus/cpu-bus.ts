import Apu from '../apu'
import Dma from '../dma'
import { Byte, Word } from '../helper'
import Keypad from '../keypad'
import Ppu from '../ppu'
import Ram from '../ram'
import Rom from '../rom'

export default class CpuBus {
  ram: Ram
  ppu: Ppu
  apu: Apu
  programROM: Rom
  keypad: Keypad
  dma: Dma

  constructor (ram: Ram, programROM: Rom, ppu: Ppu, keypad: Keypad, dma: Dma, apu: Apu) {
    this.ram = ram
    this.programROM = programROM
    this.ppu = ppu
    this.apu = apu
    this.keypad = keypad
    this.dma = dma
  }

  readByCpu (addr: Word): Byte {
    if (addr < 0x0800) return this.ram.read(addr)
    // mirror
    else if (addr < 0x2000) return this.ram.read(addr - 0x0800)
    else if (addr < 0x4000) return this.ppu.read((addr - 0x2000) % 8)
    // TODO Add 2p
    else if (addr === 0x4016) return +this.keypad.read()
    // Mirror, if prom block number equals 1
    else if (addr >= 0xC000) {
      return this.programROM.size <= 0x4000
        ? this.programROM.read(addr - 0xC000)
        : this.programROM.read(addr - 0x8000)
    } else if (addr >= 0x8000) return this.programROM.read(addr = 0x8000) // ROM
    // FIXME:
    else return 0
  }

  writeByCpu (addr: Word, data: Byte) {
    // RAM
    if (addr < 0x0800) this.ram.write(addr, data)
    // Mirror
    else if (addr < 0x2000) this.ram.write(addr - 0x0800, data)
    // PPU
    else if (addr < 0x2008) this.ppu.write(addr - 0x2000, data)
    else if (addr >= 0x4000 && addr < 0x4020) {
      if (addr === 0x4014) this.dma.write(data)
      // TODO ADD P2
      else if (addr === 0x4016) this.keypad.write(data)
      // APU
      else this.apu.write(addr - 0x4000, data)
    }
  }
}
