import { Sprite } from '../ppu'

export function sprites2css (sprites: Sprite[]): string {
  let cssString = ''
  for (let i = 0; i < sprites.length; i++) cssString += renderSprite(sprites[i], i)
  return cssString
}

function renderSprite (sprite: Sprite, tileNumber: number): string {
  let spriteString = ''
  for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
      const x = j + (tileNumber % 32) * 8
      const y = i + ~~(tileNumber / 32) * 8
      const color = sprite[i][j] * 85
      if (x === 255 && y === 239) {
        spriteString += `${x}px ${y}px rgb(${color},${color},${color})`
      } else {
        spriteString += `${x}px ${y}px rgb(${color},${color},${color}), `
      }
    }
  }
  return spriteString
}
