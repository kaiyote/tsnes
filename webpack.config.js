const { join } = require('path')
const webpack = require('webpack')

module.exports = {
  mode: 'development',
  devtool: 'eval',
  entry: [
    'webpack-dev-server/client?http://localhost:8888',
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin()
  ],
  module: {
    rules: [{
      test: /\.ts$/,
      use: [
        { loader: 'ts-loader' }
      ]
    }]
  },
  resolve: {
    extensions: ['.js', '.ts']
  }
}
