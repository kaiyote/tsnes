export type Mock < T, Omit extends keyof T = never > = {
  [Member in Exclude<keyof T, Omit>]: T[Member]
}
