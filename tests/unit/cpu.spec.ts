import { Expect, Setup, Test, TestFixture } from 'alsatian'
import CpuBus from '../../src/bus/cpu-bus'
import Cpu from '../../src/cpu'
import * as op from '../../src/cpu/opcode'
import Interrupts from '../../src/interrupts'
import Ram from '../../src/ram'
import Rom from '../../src/rom'
import MockCpuBus from './MockCpuBus'
import MockInterrupts from './MockInterrupts'
import MockRom from './MockRom'

const defaultRegisters = {
  A: 0x00,
  X: 0x00,
  Y: 0x00,
  P: {
    negative: false,
    overflow: false,
    reserved: true,
    break: true,
    decimal: false,
    interrupt: true,
    zero: false,
    carry: false
  },
  SP: 0x01FD,
  PC: 0x0000
}

@TestFixture('Cpu Test')
export class CpuTests {
  cpu?: Cpu
  bus?: CpuBus
  rom?: Rom
  memory?: Ram
  interrupts?: Interrupts

  @Setup
  setup () {
    this.memory = new Ram(0x10000)
    this.bus = new MockCpuBus(this.memory, new MockRom() as Rom) as unknown as CpuBus
    this.interrupts = new MockInterrupts() as unknown as Interrupts
    this.cpu = new Cpu(this.bus, this.interrupts)
    this.cpu.registers.PC = 0x8000
  }

  @Test('LDA_IMM')
  ldaImmTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_IMM, 0xAA]))
    this.bus && (this.bus.programROM = this.rom)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      A: 0xAA
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_ZERO')
  ldaZeroTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      A: 0xAA
    }
    Expect(cycle).toBe(3)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_ZEROX')
  ldaZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_ZEROX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0xAA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      A: 0xAA,
      X: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_ABS')
  ldaAbsTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      A: 0xAA
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_ABSX')
  ldaAbsxTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0x10AA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      A: 0xAA,
      X: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_ABSY')
  ldaAbsyTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_ABSY, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0x05)
    this.memory && this.memory.write(0x10AA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      A: 0xAA,
      Y: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_INDX')
  ldaIndxTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_INDX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0xAA, 0xA0)
    this.memory && this.memory.write(0xA0, 0xDE)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      A: 0xDE,
      X: 0x05
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDA_INDY')
  ldaIndyTest () {
    this.rom = new Rom(new Uint8Array([op.LDA_INDY, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0x05)
    this.memory && this.memory.write(0xA5, 0xA0)
    this.memory && this.memory.write(0xA6, 0x10)
    this.memory && this.memory.write(0x10A5, 0xDE)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      A: 0xDE,
      Y: 0x05
    }
    Expect(cycle).toBe(5)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDX_IMM')
  ldxImmTest () {
    this.rom = new Rom(new Uint8Array([op.LDX_IMM, 0xAA]))
    this.bus && (this.bus.programROM = this.rom)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      X: 0xAA
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDX_ZERO')
  ldxZeroTest () {
    this.rom = new Rom(new Uint8Array([op.LDX_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      X: 0xAA
    }
    Expect(cycle).toBe(3)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDX_ZEROY')
  ldxZeroyTest () {
    this.rom = new Rom(new Uint8Array([op.LDX_ZEROY, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0x05)
    this.memory && this.memory.write(0xAA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      X: 0xAA,
      Y: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDX_ABS')
  ldxAbsTest () {
    this.rom = new Rom(new Uint8Array([op.LDX_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      X: 0xAA
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDX_ABSY')
  ldxAbsyTest () {
    this.rom = new Rom(new Uint8Array([op.LDX_ABSY, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0x05)
    this.memory && this.memory.write(0x10AA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      X: 0xAA,
      Y: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDY_IMM')
  ldyImm () {
    this.rom = new Rom(new Uint8Array([op.LDY_IMM, 0xAA]))
    this.bus && (this.bus.programROM = this.rom)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      Y: 0xAA
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDY_ZERO')
  ldyZeroTest () {
    this.rom = new Rom(new Uint8Array([op.LDY_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      Y: 0xAA
    }
    Expect(cycle).toBe(3)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDY_ZEROX')
  ldyZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.LDY_ZEROX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0xAA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8002,
      Y: 0xAA,
      X: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDY_ABS')
  ldyAbsTest () {
    this.rom = new Rom(new Uint8Array([op.LDY_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      Y: 0xAA
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LDY_ABSX')
  ldyAbsxTest () {
    this.rom = new Rom(new Uint8Array([op.LDY_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0x10AA, 0xAA)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true },
      PC: 0x8003,
      Y: 0xAA,
      X: 0x05
    }
    Expect(cycle).toBe(4)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('STA_ZERO')
  staZeroTest () {
    this.rom = new Rom(new Uint8Array([op.STA_ZERO, 0xDE]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0xDE)
    Expect(cycle).toBe(3)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_ZEROX')
  staZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.STA_ZEROX, 0xA0]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0xA5)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_ABS')
  staAbsTest () {
    this.rom = new Rom(new Uint8Array([op.STA_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10A5)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_ABSX without page cross')
  staAbsxTest1 () {
    this.rom = new Rom(new Uint8Array([op.STA_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10AA)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_ABSX with page cross')
  staAbsxTest2 () {
    this.rom = new Rom(new Uint8Array([op.STA_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x65)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x110A)
    Expect(cycle).toBe(5)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_ABSY without page cross')
  staAbsyTest1 () {
    this.rom = new Rom(new Uint8Array([op.STA_ABSY, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0x05)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10AA)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_ABSY with page cross')
  staAbsyTest2 () {
    this.rom = new Rom(new Uint8Array([op.STA_ABSY, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0x65)
    this.cpu && (this.cpu.registers.A = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x110A)
    Expect(cycle).toBe(5)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_INDX')
  staIndxTest () {
    this.rom = new Rom(new Uint8Array([op.STA_INDX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xA5)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0xAA, 0xDE)
    this.memory && this.memory.write(0xAB, 0x10)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10DE)
    Expect(cycle).toBe(7)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_INDY without page cross')
  staIndyTest1 () {
    this.rom = new Rom(new Uint8Array([op.STA_INDY, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xA5)
    this.cpu && (this.cpu.registers.Y = 0x05)
    this.memory && this.memory.write(0xA5, 0xDE)
    this.memory && this.memory.write(0xA6, 0x10)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10E3)
    Expect(cycle).toBe(6)
    Expect(data).toEqual(0xA5)
  }

  @Test('STA_INDY with page cross')
  staIndyTest2 () {
    this.rom = new Rom(new Uint8Array([op.STA_INDY, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xA5)
    this.cpu && (this.cpu.registers.Y = 0x25)
    this.memory && this.memory.write(0xA5, 0xDE)
    this.memory && this.memory.write(0xA6, 0x10)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x1103)
    Expect(cycle).toBe(7)
    Expect(data).toEqual(0xA5)
  }

  @Test('STX_ZERO')
  stxZeroTest () {
    this.rom = new Rom(new Uint8Array([op.STX_ZERO, 0xDE]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0xDE)
    Expect(cycle).toBe(3)
    Expect(data).toEqual(0xA5)
  }

  @Test('STX_ZEROY')
  stxZeroyTest () {
    this.rom = new Rom(new Uint8Array([op.STX_ZEROY, 0xA0]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0xA5)
    this.cpu && (this.cpu.registers.Y = 0x05)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0xA5)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STX_ABS')
  stxAbsTest () {
    this.rom = new Rom(new Uint8Array([op.STX_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10A5)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STY_ZERO')
  styZeroTest () {
    this.rom = new Rom(new Uint8Array([op.STY_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0xA5)
    Expect(cycle).toBe(3)
    Expect(data).toEqual(0xA5)
  }

  @Test('STY_ZEROX')
  styZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.STY_ZEROX, 0xA0]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.cpu && (this.cpu.registers.Y = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0xA5)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('STY_ABS')
  styAbsTest () {
    this.rom = new Rom(new Uint8Array([op.STY_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.Y = 0xA5)
    const cycle = this.cpu && this.cpu.run()
    const data = this.memory && this.memory.read(0x10A5)
    Expect(cycle).toBe(4)
    Expect(data).toEqual(0xA5)
  }

  @Test('SEI')
  seiTest () {
    this.rom = new Rom(new Uint8Array([op.SEI]))
    this.bus && (this.bus.programROM = this.rom)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, interrupt: true },
      PC: 0x8001
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('CLI')
  cliTest () {
    this.rom = new Rom(new Uint8Array([op.CLI]))
    this.bus && (this.bus.programROM = this.rom)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, interrupt: false },
      PC: 0x8001
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LSR')
  lsrTest () {
    this.rom = new Rom(new Uint8Array([op.LSR]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xa5)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8001,
      A: 0x52
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('LSR_ZERO')
  lsrZeroTest () {
    this.rom = new Rom(new Uint8Array([op.LSR_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(5)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xA5)).toBe(0x77)
  }

  @Test('LSR_ZEROX')
  lsrZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.LSR_ZEROX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xAA, 0xEF)
    this.cpu && (this.cpu.registers.X = 0x05)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      X: 0x05,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xAA)).toBe(0x77)
  }

  @Test('LSR_ABS')
  lsrAbsTest () {
    this.rom = new Rom(new Uint8Array([op.LSR_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8003
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10A5)).toBe(0x77)
  }

  @Test('LSR_ABSX without page cross')
  lsrAbsxTest1 () {
    this.rom = new Rom(new Uint8Array([op.LSR_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0x10AA, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8003,
      X: 0x05
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10AA)).toBe(0x77)
  }

  @Test('LSR_ABSX with page cross')
  lsrAbsxTest2 () {
    this.rom = new Rom(new Uint8Array([op.LSR_ABSX, 0x01, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0xFF)
    this.memory && this.memory.write(0x1100, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8003,
      X: 0xFF
    }
    Expect(cycle).toBe(7)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x1100)).toBe(0x77)
  }

  @Test('ASL')
  aslTest () {
    this.rom = new Rom(new Uint8Array([op.ASL]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xa5)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true },
      PC: 0x8001,
      A: 0x4A
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('ASL_ZERO')
  aslZeroTest () {
    this.rom = new Rom(new Uint8Array([op.ASL_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, carry: true, negative: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(5)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xA5)).toBe(0xDE)
  }

  @Test('ASL_ZEROX')
  aslZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.ASL_ZEROX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xAA, 0xEF)
    this.cpu && (this.cpu.registers.X = 0x05)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      X: 0x05,
      P: { ...defaultRegisters.P, negative: true, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xAA)).toBe(0xDE)
  }

  @Test('ASL_ABS')
  aslAbsTest () {
    this.rom = new Rom(new Uint8Array([op.ASL_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true, carry: true },
      PC: 0x8003
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10A5)).toBe(0xDE)
  }

  @Test('ASL_ABSX without page cross')
  aslAbsxTest1 () {
    this.rom = new Rom(new Uint8Array([op.ASL_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0x10AA, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true, carry: true },
      PC: 0x8003,
      X: 0x05
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10AA)).toBe(0xDE)
  }

  @Test('ASL_ABSX with page cross')
  aslAbsxTest2 () {
    this.rom = new Rom(new Uint8Array([op.ASL_ABSX, 0x01, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.X = 0xFF)
    this.memory && this.memory.write(0x1100, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true, carry: true },
      PC: 0x8003,
      X: 0xFF
    }
    Expect(cycle).toBe(7)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x1100)).toBe(0xDE)
  }

  @Test('ROR')
  rorTest () {
    this.rom = new Rom(new Uint8Array([op.ROR]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.A = 0xa5)
    this.cpu && (this.cpu.registers.P.carry = true)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: true, carry: true },
      PC: 0x8001,
      A: 0xD2
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('ROR_ZERO')
  rorZeroTest () {
    this.rom = new Rom(new Uint8Array([op.ROR_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xEF)
    this.cpu && (this.cpu.registers.P.carry = true)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(5)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xA5)).toBe(0xF7)
  }

  @Test('ROR_ZEROX')
  rorZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.ROR_ZEROX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xAA, 0xEF)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.X = 0x05)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      X: 0x05,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xAA)).toBe(0xF7)
  }

  @Test('ROR_ABS')
  rorAbsTest () {
    this.rom = new Rom(new Uint8Array([op.ROR_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xEF)
    this.cpu && (this.cpu.registers.P.carry = true)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8003
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10A5)).toBe(0xF7)
  }

  @Test('ROR_ABSX without page cross')
  rorAbsxTest1 () {
    this.rom = new Rom(new Uint8Array([op.ROR_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0x10AA, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8003,
      X: 0x05
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10AA)).toBe(0xF7)
  }

  @Test('ROR_ABSX with page cross')
  rorAbsxTest2 () {
    this.rom = new Rom(new Uint8Array([op.ROR_ABSX, 0x01, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.X = 0xFF)
    this.memory && this.memory.write(0x1100, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8003,
      X: 0xFF
    }
    Expect(cycle).toBe(7)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x1100)).toBe(0xF7)
  }

  @Test('ROL')
  rolTest () {
    this.rom = new Rom(new Uint8Array([op.ROL]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.A = 0xa5)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, negative: false, carry: true },
      PC: 0x8001,
      A: 0x4B
    }
    Expect(cycle).toBe(2)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
  }

  @Test('ROL_ZERO')
  rolZeroTest () {
    this.rom = new Rom(new Uint8Array([op.ROL_ZERO, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xA5, 0xEF)
    this.cpu && (this.cpu.registers.P.carry = true)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(5)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xA5)).toBe(0xDF)
  }

  @Test('ROL_ZEROX')
  rolZeroxTest () {
    this.rom = new Rom(new Uint8Array([op.ROL_ZEROX, 0xA5]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0xAA, 0xEF)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.X = 0x05)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      X: 0x05,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8002
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0xAA)).toBe(0xDF)
  }

  @Test('ROL_ABS')
  rolAbsTest () {
    this.rom = new Rom(new Uint8Array([op.ROL_ABS, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.memory && this.memory.write(0x10A5, 0xEF)
    this.cpu && (this.cpu.registers.P.carry = true)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8003
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10A5)).toBe(0xDF)
  }

  @Test('ROL_ABSX without page cross')
  rolAbsxTest1 () {
    this.rom = new Rom(new Uint8Array([op.ROL_ABSX, 0xA5, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.X = 0x05)
    this.memory && this.memory.write(0x10AA, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8003,
      X: 0x05
    }
    Expect(cycle).toBe(6)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x10AA)).toBe(0xDF)
  }

  @Test('ROL_ABSX with page cross')
  rolAbsxTest2 () {
    this.rom = new Rom(new Uint8Array([op.ROL_ABSX, 0x01, 0x10]))
    this.bus && (this.bus.programROM = this.rom)
    this.cpu && (this.cpu.registers.P.carry = true)
    this.cpu && (this.cpu.registers.X = 0xFF)
    this.memory && this.memory.write(0x1100, 0xEF)
    const cycle = this.cpu && this.cpu.run()
    const expected = {
      ...defaultRegisters,
      P: { ...defaultRegisters.P, zero: false, negative: true, carry: true },
      PC: 0x8003,
      X: 0xFF
    }
    Expect(cycle).toBe(7)
    Expect(this.cpu && this.cpu.registers).toEqual(expected)
    Expect(this.memory && this.memory.read(0x1100)).toBe(0xDF)
  }
}
