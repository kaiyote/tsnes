import CpuBus from '../../src/bus/cpu-bus'
import { Byte, Word } from '../../src/helper'
import Ram from '../../src/ram'
import Rom from '../../src/rom'
import { Mock } from '../Mock'

// Mock
export default class MockCpuBus implements Mock<CpuBus, 'ppu' | 'apu' | 'keypad' | 'dma'> {
  ram: Ram
  programROM: Rom
  constructor (ram: Ram, programROM: Rom) {
    this.ram = ram
    this.programROM = programROM
  }

  readByCpu (addr: Word): Byte {
    if (addr >= 0x8000) {
      return this.programROM.read(addr - 0x8000)
    }
    return this.ram.read(addr)
  }

  writeByCpu (addr: Word, data: Byte) {
    this.ram.write(addr, data)
  }
}
