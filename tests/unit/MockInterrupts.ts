import Interrupts from '../../src/interrupts'
import { Mock } from '../Mock'

export default class MockInterrupts
  implements Mock<Interrupts, 'nmi' | 'irq' | 'isIrqAssert' | 'assertNmi' | 'assertIrq' | 'deassertIrq'> {
  isNmiAssert: boolean = false

  // tslint:disable-next-line:no-empty
  deassertNmi = () => { }
}
