import Rom from '../../src/rom'
import { Mock } from '../Mock'

export default class MockRom implements Mock<Rom, 'rom' | 'size' | 'read'> {
  // empty on purpose
}
